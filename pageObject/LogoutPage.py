from selenium.webdriver.common.by import By
from Locators.Locators import Locator as ele
from Configuration.config import TestData
from pageObject.BasePage import BasePage
from pageObject.HomePage import HomePage
import time
from Utility.customLogger import LogGen

class LogoutPage(BasePage):
    def __init__(self,driver):
        super().__init__(driver)
        time.sleep(5)
        self.driver.get(TestData.BASE_URL)

    def clickLogoutDropdown(self):
        self.do_click(ele.logout_dropdown_xpath)

    def clickLogoutLink(self):
        self.do_click(ele.link_logout_linktext)




