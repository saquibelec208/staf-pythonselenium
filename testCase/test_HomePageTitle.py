import pytest
from allure_commons.types import AttachmentType

from Configuration.config import TestData
from pageObject.LoginPage import LoginPage
from testCase.test_Base import BaseTest
from Utility.customLogger import LogGen
import allure


class Test_001_Login(BaseTest):

    logger = LogGen.loggen()

    @pytest.mark.sanity
    @pytest.mark.regression
    def test_home_page_title(self):

        self.logger.info("*************** Test_001 Verifying Home Page Test *****************")
        self.logger.info("****Started Home page title test ****")
        self.loginPage=LoginPage(self.driver)
        self.logger.info("****Opening URL****")
        title = self.loginPage.get_title(TestData.LOGIN_PAGE_TITLE)
        if title == TestData.LOGIN_PAGE_TITLE:
            self.logger.info("**** Home page title test passed ****")
            assert True
        else:
            #self.driver.save_screenshot(".\\Screenshot\\" + "test_home_page_title.png")
            allure.attach(self.driver.get_screenshot_as_png(), name='homePageTitle',
                          attachment_type=AttachmentType.PNG)
            self.logger.error("**** Home page title test failed****")
            assert False
