import pytest
from allure_commons.types import AttachmentType

from Configuration.config import TestData
from pageObject.LoginPage import LoginPage
from pageObject.LogoutPage import LogoutPage
from testCase.test_Base import BaseTest
from Utility.customLogger import LogGen
import time
import allure


class Test_001_Login(BaseTest):

    logger = LogGen.loggen()

    @pytest.mark.sanity
    @pytest.mark.regression
    def test_login(self):
        self.logger.info("***********TC_0012 Verifying Logout Test***********")
        self.logger.info("****Started Logout Test****")
        self.loginPage = LoginPage(self.driver)
        self.logoutPage = LogoutPage(self.driver)


        self.loginPage.setUserName(TestData.USER_NAME)
        self.loginPage.setPassword(TestData.PASSWORD)
        self.loginPage.clickLogin()

        time.sleep(5)
        act_title=self.driver.title
        #print("title->>"+act_title)
        self.logger.info("****Logout Page title -->>"+act_title)

        self.logoutPage.clickLogoutDropdown()
        time.sleep(2)
        self.logoutPage.clickLogoutLink()
        time.sleep(5)
        if act_title == TestData.HOME_PAGE_TITLE:
            self.logger.info("****Logout test passed ****")
            assert True
        else:
            self.logger.error("****Logout test failed ****")
            #self.driver.save_screenshot(".\\Screenshot\\" + "test_login.png")
            allure.attach(self.driver.get_screenshot_as_png(), name='testLogout',
                          attachment_type=AttachmentType.PNG)
            assert False




