from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.chrome.service import Service
import pytest
from selenium.webdriver import Remote

@pytest.fixture(params=["chrome"],scope='class')
def setup_driver(request):
    if request.param == "chrome":
        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("start-maximized")
        options.add_argument("disable-infobars")
        options.add_argument("--disable-extensions")
        # web_driver = webdriver.Chrome(service= Service(ChromeDriverManager().install()))
        web_driver = webdriver.Remote(options=options,
                                      command_executor='http://selenium__standalone-chrome:4444/wd/hub')
    if request.param=="firefox":
        # web_driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
        web_driver = webdriver.Remote(options=webdriver.FirefoxOptions(),
                               command_executor='http://selenium__standalone-firefox:4444/wd/hub')
    if request.param=="edge":
        web_driver = webdriver.Edge(service=Service(ChromeDriverManager.install()))

    request.cls.driver = web_driver
    yield
    web_driver.close()

@pytest.fixture()
def setup(browser)-> Remote:
    if browser=='chrome':
        #driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
        #print("Launching chrome browser.........")
        driver = webdriver.Remote(options=webdriver.ChromeOptions(),command_executor='http://selenium__standalone-chrome:4444/wd/hub')

    elif browser=='firefox':
        #driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
        #print("Launching firefox browser.........")
        driver = webdriver.Remote(options=webdriver.FirefoxOptions(),command_executor='http://selenium__standalone-firefox:4444/wd/hub')
    return driver






def pytest_addoption(parser):    # This will get the value from CLI /hooks
    parser.addoption("--browser",default='chrome')

@pytest.fixture()
def browser(request):  # This will return the Browser value to setup method
    return request.config.getoption("--browser")


